#!/bin/bash

TYPE=$1

case $TYPE in
        "package") echo "package code.tar"
                #    tar -cf code.tar code/
                #    \cp code.tar ansible/roles/deploy/files
                   exit 0
                   ;;
        "prod")    echo "trigger production branch"
                #    ansible-playbook -i ansible/inventories/prod/hosts ansible/deploy_prod.yml
                   exit 0
                   ;;
        "sandbox") echo "trigger sandbox branch"
                #    ansible-playbook -i ansible/inventories/sandbox/hosts ansible/deploy_sandbox.yml
                   exit 0
                   ;;
        *)         echo "unknown command"
                   exit 1
                   ;;
esac
