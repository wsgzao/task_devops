# !/usr/bin/python2
import pymysql
import ConfigParser

config = ConfigParser.ConfigParser()
config.read('conf.ini')

def pyconn():
    conn = pymysql.connect(host=config.get('baseconf', 'host'),
                           user=config.get('baseconf', 'user'),
                           password=config.get('baseconf', 'password'),
                           database=config.get('baseconf', 'database'),
                           port=int(config.get('baseconf', 'port')))
    cursor = conn.cursor()
    cursor.execute("select version()")
    data = cursor.fetchone()
    print("Database Version: %s" % data)
    conn.close()

if __name__ == "__main__":
    pyconn()
