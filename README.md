# task_devops

## Environment

local environment:
- macOS Mojave
- Homebrew
- Docker
- Python 2.7
- Ansible 
- MySQL 8

create local_init_macos.sh script

```bash
#!/bin/bash

# install homebrew
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"

# install docker
brew cask install docker

# install mysql-client
brew install mysql-client
# set mysql-client env
echo 'export PATH="/usr/local/opt/mysql-client/bin:$PATH"' >> ~/.zshrc
source ~/.zshrc

# create directory
mkdir -p ~/mysql_data/8

# install mysql-server
docker run --name mysql-8 \
-p 3306:3306 \
-v ~/mysql_data/8:/var/lib/mysql \
-e MYSQL_ROOT_PASSWORD=root \
-e TZ='Asia/Singapore' \
--restart=always \
-d mysql:8 \
--character-set-server=utf8mb4 \
--collation-server=utf8mb4_general_ci

# test mysql connection
mysql -h 127.0.0.1 -uroot -proot -P 3306

# set mysql:8 mysql_native_password if we need
mysql> show databases;
mysql> use mysql;
mysql> select host,user,plugin from user;
mysql> alter user 'root'@'%' identified with mysql_native_password by 'root';

```

## Task 1

You are required to
1. Create a small application in any language that uses MySQL with 1 test.

**Answer**

```bash
# Drop support for Python 2. Setuptools now requires Python 3.5 or later. Install setuptools using pip >=9 or pin to Setuptools <45 to maintain 2.7 support.
pip install setuptools==44
pip install pymysql
```

```python
# !/usr/bin/python2
import pymysql

def pyconn():
    conn = pymysql.connect(host="127.0.0.1", 
                           user="root",
                           password="root",
                           database="mysql", 
                           port=3306)
    cursor = conn.cursor()
    cursor.execute("select version()")
    data=cursor.fetchone()
    print("Database Version: %s" %data)
    conn.close()

if __name__=="__main__":
    pyconn()

```

python mysql_connect.py
Database Version: 8.0.20

## Task 2

By using your environment, the developer should be able to:
1. Automate building of local environment on Ubuntu or mac that include a MySQL server and an application server. The developer should be easy to change the code base in the application and test if it works locally in an efficient manner.
2. Use a script to deploy the application to sandbox and prod environment which is scalable (deployable across multiple machines). On the command line, it would run

```bash
git push devops production   // to push and deploy to production environment
git push devops sandbox      // to push and deploy to sandbox environment
```

You may assume there are 2 application servers minimum, a database server, and a load balancer in front and your script will be able to create those on clean Ubuntu servers.

**Answer**

Assume these Ubuntu servers are clean, we need to set up some basic configurations for Ansible automated management in advance.
- sudo apt-get install openssh-server python -y
- update firewall(ufw/iptables) policy
- ssh-copy-id [-i [identity_file]] [user@]hostname
- add sudoers users to wheel group
- ansible -i [hosts] all -m ping

After we finish the above steps, we can do the next tasks
1. check `local_init_macos.sh` and `code` directory on macOS
2. check `ansible` directory, update inventories/hosts and group_vars/[all|app] 

More details can refer [LAMP Stack + HAProxy: Example Playbooks](https://github.com/ansible/ansible-examples/tree/master/lamp_haproxy)


## Task 3

1. Run the test on a CI server after the code is committed via git.

**Answer**

1. The `.gitlab-ci.yml` file tells the GitLab Runner what to do. A simple pipeline commonly has three stages(build, test, deploy)
2. Ensure project is configured to use a [Runner](https://docs.gitlab.com/ce/ci/quick_start/README.html#configuring-a-runner).
3. Turn on webhook trigger push events for branch name `production` and `sandbox`

[GitLab CI/CD Pipeline Configuration Reference](https://docs.gitlab.com/ce/ci/yaml/README.html)

## Task 4

1. Secure your production Ubuntu server and your DevOps pipeline. Answer the following questions: How do you harden your Ubuntu Linux server against penetration and attacks? How do you secure your MySQL database? What are security concerns when setting up your DevOps pipeline?

**Answer**

How do you harden your Ubuntu Linux server against penetration and attacks?
1. Mininal install, disable services that we do not need
2. Disable password login, use ssh public key authentication, use OpenLDAP instead of local management
3. Update ufw/iptables policy, add whitelist for management. Drop packets from spoofed networks, Drop Invalid packets, Drop Bogus TCP packets
4. Enable audit log, use rsyslog/ELK or other methods collect log, non-sensitive data by machine rules, sensitive data by human review, if have Jump Server should be a better way
5. Subscribe security information, install security updates when you're notified, set up os monitoring platform use Nmap or other advanced tools make periodic security scan report
6. Backup and reestore policy, make sure it is effective
7. Set up OS SOP, use Ansible or other tools help to do osinit-pipeline and make periodic manual security hardening review report 

[Ubuntu security notices](https://usn.ubuntu.com/)

[CIS Ubuntu Linux Benchmarks](https://www.cisecurity.org/benchmark/ubuntu_linux/)

[DevSec Linux Security Baseline](https://dev-sec.io/baselines/linux/)

How do you secure your MySQL database?
1. mysql_secure_installation(Remove anonymous users, Disallow root login remotely, Remove test database and access to it, Change default passwd)
2. Update firewall policy, allow network access based on white-list approach only
3. Enable MySQL audit plugin, record and analyse users sensitive command, set up database monitoring platform
4. Enable MySQL "master-slave" replication, set up full/incremental backup policy
5. Set up DB SOP, use Ansible or other tools help to do dbinit-pipeline and make periodic manual security hardening review report 

[DevSec MySQL Baseline](https://dev-sec.io/baselines/mysql/)

What are security concerns when setting up your DevOps pipeline?
1. DevSecOps Pipeline usually have 5 stage(Code, Manage, Store, Build, Deploy)
2. SonarQube can help to make Code Quality and Security on Static Code Analysis
3. OpenSCAP can help to make security scan after build stage
4. OWASP ZAP can helo to make web app scan after deploy stage

[OWASP Appsec Pipeline](https://owasp.org/www-project-appsec-pipeline/)

![](https://raw.githubusercontent.com/wsgzao/storage-public/master/img/20200510150829.png)

## Reference

![](https://raw.githubusercontent.com/wsgzao/storage-public/master/img/20200510150706.png)

[How to Build an Effective Initial Deployment Pipeline](https://www.toptal.com/devops/effective-ci-cd-deployment-pipeline)

[Basic Steps for MySQL Server Deployment with Docker](https://dev.mysql.com/doc/refman/8.0/en/docker-mysql-getting-started.html)

[DevSecOps：打造安全合规的 DevOps 平台](https://www.ibm.com/developerworks/cn/cloud/library/cl-lo-devsecops-build-a-devops-platform-for-security-compliance/index.html)

