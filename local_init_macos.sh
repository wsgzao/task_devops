#!/bin/bash

# install homebrew
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"

# install docker
brew cask install docker

# install mysql-client
brew install mysql-client
# set mysql-client env
echo 'export PATH="/usr/local/opt/mysql-client/bin:$PATH"' >> ~/.zshrc
source ~/.zshrc

# create directory
mkdir -p ~/mysql_data/8

# install mysql-server
docker run --name mysql-8 \
-p 3306:3306 \
-v ~/mysql_data/8:/var/lib/mysql \
-e MYSQL_ROOT_PASSWORD=root \
-e TZ='Asia/Singapore' \
--restart=always \
-d mysql:8 \
--character-set-server=utf8mb4 \
--collation-server=utf8mb4_general_ci

# test mysql connection
mysql -h 127.0.0.1 -uroot -proot -P 3306